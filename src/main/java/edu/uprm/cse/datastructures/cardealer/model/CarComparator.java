package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{

	/*This method compares two cars in order to determine which one is greater.
	 *It is based on [1] brand, [2] model, and [3] model option. 
	 */

	public int compare(Car car1, Car car2) {
		//If car1 is equal to car2, then the cars' models will define which one goes first. 
		if (car1.getCarBrand().compareTo(car2.getCarBrand())==0) { 
			if (car1.getCarModel().compareTo(car2.getCarModel())==0) {
				//If it cannot be decided by the car models because they are equal, 
				//the model options will decide.
				return car1.getCarModelOption().compareTo(car2.getCarModelOption());
			}
			return car1.getCarModel().compareTo(car2.getCarModel());
		}
		return car1.getCarBrand().compareTo(car2.getCarBrand());
	}


}

