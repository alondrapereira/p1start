package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import edu.uprm.cse.datastructures.cardealer.model.*;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {

	private static CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance();

	
	//Method returns an array of all the cars in cList. 
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getCarList(){ 
		if(cList.isEmpty()) {
			return null;
		}
		Car[] arr = new Car[cList.size()];
		for(int i=0;i<arr.length; i++) {
			arr[i]=(Car)cList.get(i);
		}
		return arr;
	} 

	//Method returns a the car that possesses the given ID. 
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){ 
		for(int i=0; i<cList.size(); i++){
			if(((Car)cList.get(i)).getCarId()==id){ 
				return (Car)cList.get(i);
			}  
		}  throw new NotFoundException();
	}

	//Method returns 201 if the adding was successful or 404, otherwise. 
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		for(int i=0; i<cList.size(); i++){
			if(((Car)cList.get(i)).getCarId()==car.getCarId() || 
					((Car)cList.get(i)).getCarBrand()==null || 
					((Car)cList.get(i)).getCarModel()==null || 
					((Car)cList.get(i)).getCarModelOption()==null){

				return Response.status(404).build();
			}
		}
		cList.add(car);
		return Response.status(201).build();
	}
	
	//Method exchanges a car to a new one with the same id. 
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){
		for(int i=0; i<cList.size(); i++){
			if(((Car)cList.get(i)).getCarId()==car.getCarId()){ 
				cList.remove(i); //removes the old car
				cList.add(car); //adds the new car
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(404).build();
	}

	//Method removes a Car of a given ID from the list 
	//Returns if the operation was successful or not (404). 
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		for(int i=0; i<cList.size(); i++){
			if(id==((Car)cList.get(i)).getCarId()){
				cList.remove(i);
				return Response.status(Response.Status.OK).build();
			}
		}	
		return Response.status(404).build();

	}

}