package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

//This class creates a CSDLL list that have two methods : getInstance() and resetCars().
public class CarList {

	private static CircularSortedDoublyLinkedList<Car> cList = new CircularSortedDoublyLinkedList<Car> (new CarComparator());

	//Returns an instance of the cList. 
	public static CircularSortedDoublyLinkedList<Car> getInstance(){
		return cList; 
	}

	//Clears the cList.
	public static void resetCars() {
		cList.clear();	
	}

}
