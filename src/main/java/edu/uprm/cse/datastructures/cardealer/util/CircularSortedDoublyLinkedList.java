package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.CarComparator;


/* This class presents the implementation of the Circular Sorted Doubly Linked List (CSDLL) 
 * by Alondra N. Pereira Roman
 */

public class CircularSortedDoublyLinkedList <E> implements SortedList<E>{

	//Class that includes all the Nodes' methods. 
	private static class Node<E> {
		//Instance variables
		private E element;
		private Node<E> next;
		private Node<E> previous;

		//Constructors
		public Node(E element, Node<E> next, Node<E> previous) {
			super();
			this.element = element;
			this.next = next;
			this.previous = previous; 
		}
		public Node() {
			super();
		}

		//Getters and Setters
		public E getElement() {
			return element;
		}
		public void setElement(E e) {
			this.element = e;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrevious() {
			return previous;
		}
		public void setPrevious(Node<E> prev) {
			this.previous = prev;
		}


	}

	//Instance variables
	private Node<E> header;
	private int currentSize;
	private Comparator<E> comp; 

	//CSDLL constructor using a CarComparator type comparator.
	public CircularSortedDoublyLinkedList(CarComparator comp) {
		this.header = new Node<>(); 
		//This makes the header to point itself, when the list is empty.
		header.setPrevious(header);
		header.setNext(header);
		this.currentSize = 0;
		this.comp = (Comparator<E>) comp;
	}

	//CSDLL constructor using a generic comparator.
	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		this.header = new Node<E>();
		//This makes the header to point itself, when the list is empty.
		this.header.setPrevious(this.header);
		this.header.setNext(this.header);
		this.currentSize = 0;
		this.comp = comp;
	}


	//This class includes all the CSDLL iterator methods. 
	private class CircularSortedDoublyLinkedListIterator <E> implements Iterator<E> {
		//Instance variables
		private Node <E> nextNode; 
		private Node <E> previousNode; 

		//Constructor
		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
			this.previousNode = (Node<E>) header.getPrevious();
		}
		//Methods
		@Override 

		//Checks if the node has a following node.
		public boolean hasNext() {
			return nextNode != header;
		}

		//Checks if the node has a previous node. 
		public boolean hasPrevious() {
			return previousNode != header; 
		}

		@Override
		//Returns the element of the following node. 
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

		//Returns the element of the previous node. 
		public E previous() {
			if (this.hasPrevious()) {
				E result = this.previousNode.getElement();
				this.previousNode = this.previousNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

	}

	//Returns a new CSDLLIterator instance. 
	@Override
	public Iterator iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

	//This method is responsible for adding  new nodes to the list, without altering a specific order.
	@Override
	public boolean add(E e) {
		Node <E> newNode = new Node <E> (e, this.header, this.header);		
		//If the list is empty, the node will point the header and viceversa. 
		if (this.isEmpty()) {	
			this.header.setNext(newNode);
			this.header.setPrevious(newNode);
			currentSize++;
			return true;
		}
		else {
			Node <E> temp = this.header.getNext(); 
			while (temp != this.header) {
				/*If the given element is smaller than the temp's element, 
				 *add it before temp. Otherwise, it will keep iterating 
				 *while it finds a greater element.
				 */
				if(this.comp.compare(e, temp.getElement())<0) {
					newNode.setNext(temp);
					newNode.setPrevious(temp.getPrevious());
					temp.getPrevious().setNext(newNode);
					temp.setPrevious(newNode);
					currentSize++;
					return true;
				}
				temp=temp.getNext();
			} 	
			/*If the element is greater than every element in the list, 
			 *it should be added to the end. 
			 */
			newNode.setPrevious(header.getPrevious());
			this.header.getPrevious().setNext(newNode);
			this.header.setPrevious(newNode);
			currentSize++;
			return true;
		}
	}

	//This method returns the list's current size.
	@Override
	public int size() {
		return currentSize;
	}


	/*This method removes an element's first index 
	 *and returns true if the operation was successful 
	 *or false otherwise.
	 */

	@Override
	public boolean remove(E e) {
		try {
			return this.remove(this.firstIndex(e));
		}
		catch (IndexOutOfBoundsException ex) {
			return false; 
		}

	}

	/*This remove method erases the node at a given index. 
	 * Returns if it was possible to remove the node. 
	 */
	@Override
	public boolean remove(int index) {
		if (index>=this.currentSize|| index<0) {
			throw new IndexOutOfBoundsException();
		}

		if(!this.isEmpty()) {
			Node<E> temp = header.getNext();
			int counter=0;

			while(counter<index) {
				temp= temp.getNext();
				counter++;
			}

			Node<E> after = temp.getNext();
			Node<E> before = temp.getPrevious();
			before.setNext(after);
			after.setPrevious(before);
			this.currentSize--;
			return true;	
		}
		return false;
	}


	/*The method removes all instances of a given element and 
	 * returns the items removed. 
	 */
	@Override
	public int removeAll(E e) {
		int i = 0; 
		try {
			while (this.remove(e)) {
				i++; 
			}

		}
		catch (IndexOutOfBoundsException ex) {
			return i;
		}
		return i;

	}

	//This method intends to eliminate every node in the list. 
	@Override
	public void clear() {
		while (!this.isEmpty()) {
			this.remove(0);
		}
	}

	//This method returns the element from the node that is next to the header.
	@Override
	public E first() {
		return header.getNext().getElement();
	}

	//This method returns the element from the node that is previous to the header.
	@Override
	public E last() {
		return header.getPrevious().getElement();
	}

	//The method return the element from the node at the given index position. 
	@Override
	public E get(int index) {
		Node<E> temp = this.header.getNext();
		int i = 0; 
		if (index > currentSize -1 ) {
			return null; 
		}
		while (i != index) {
			temp = temp.getNext();
			i++; 
		}
		return temp.getElement();
	}

	//If the item has an index greater than -1, then it exists in the list. 
	@Override
	public boolean contains(E e) {
		return this.firstIndex(e)>=0;
	}

	//Returns if the list has no nodes. 
	@Override
	public boolean isEmpty() {
		return currentSize==0;
	}

	//This method searches the given element from the list and saves its first occurrence.
	//It is intended to go clockwise. 
	@Override
	public int firstIndex(E e) {
		int i = 0; //first index
		Node <E> temp = header.getNext(); 
		while (temp != header) {
			if (temp.getElement().equals(e)) {
				return i; 
			}
			temp = temp.getNext(); 
			i++; 
		}
		return -1;
	}

	//Method searches the given element from the list and saves its last occurrence. 
	//It is intended to go counterwise. 
	@Override
	public int lastIndex(E e) {
		int i = currentSize - 1; //last index
		Node <E> temp = header.getPrevious(); 
		while (temp != header) {
			if (temp.getElement().equals(e)) {
				return i; 
			}
			temp = temp.getPrevious();
			i--; 
		}
		return -1;
	}

}
